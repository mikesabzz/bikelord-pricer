// need to use something like this object in order to sub up the values and do the calculation
let bikeAttributeValueObj = {
 makeYear: $('#type-year'),
 makeInput: $('#type-make'),
 modelInput: $('#type-model')
}
 bikeAttributeValueObj

 var totalVal = 0;
 for (var key of Object.keys(bikeAttributeValueObj)) {
   totalVal += bikeAttributeValueObj[key]
  }
  totalVal

  // can you use a form to submit
  function submit() {
  //- please install JQuery and use Jquery as in below (add to head)
  //- src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"
  //- Make sure the jquery file is above your main.js file
  
  const msrp = $('#msrp').val()
  let msrpValue = Number(msrp)
  let msrpScore

  if (Number(msrp) <= 1000) {
    msrpScore = 1
  } else if (Number(msrp) > 1000 && Number(msrp) < 2000) {
    msrpScore = 2
  } else if (Number(msrp) > 2000 && Number(msrp) < 4000) {
    msrpScore = 3
  } else if (Number(msrp) > 4000 && Number(msrp) < 6000) {
    msrpScore = 4
  } else if (Number(msrp) > 6000 && Number(msrp) < 10000) {
    msrpScore = 5
  } else {
    msrpScore = 6
  }

  let max = new Date().getFullYear(),
    min = max - 10
    $(window).load(function () {
      for (let i = max; i >= min; i--) {
        let opt = new Option()
        $(opt).html(i)
        $('#type-year').append(opt)
      }
    })
    const yearSelected = $('#type-year option:selected')
    const yearValue = max - Number(yearSelected.text())

  // I would have an array that represents each number. So if you add more brands to the different arrays you can still make it work. If the brand the is selected is in the array then you assign the respective value. 

  const makeInput = $("#type-make option:selected")
  const modelInput = $('#type-model option:selected')
  const conditionInput = $('#frame-condition option:selected')
  const componentConditionInput = $('#component-condition option:selected')
  const frameInput = $('#type-frame option:selected')

  const valueArray = [Number(makeInput.val()), Number(modelInput.val()), Number(conditionInput.val()), Number(componentConditionInput.val()), Number(frameInput.val()), msrpScore, yearValue ]
  
  let averageValue = valueArray.reduce(function(a, b){
    return a + b
  }, 0)
  let total = (averageValue/7).toFixed(1)
  $('#total').html(total)
  let maxResaleValue = (msrpValue/Number(total)).toFixed(2)
  $('#resale-value').html(maxResaleValue)
  let tradeInValue = (maxResaleValue * 0.5).toFixed(2)
  $('#trade-in-value').html(tradeInValue)
  $('#expected-resale-value').html((tradeInValue*1.3).toFixed(2))
 


  // these are the only parts that you should be assigning in the HTML - the others are cool but a lot of work and not necessary for this program. 

}
submit()
